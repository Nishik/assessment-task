'use strict';

angular
    .module('myApp.dataGrid', [/*'ngRoute'*/ 'ui.bootstrap'])

    /*
     .config(['$routeProvider', function($routeProvider) {
     $routeProvider.when('/dataGrid', {
     templateUrl: 'dataGrid/dataGrid.html',
     controller: 'dataGridCtrl'
     });
     }])*/

    .factory('dataStorage', ['$http', function ($http) {

        var thePromise = $http.get('Storage/players.json')
            .then(function (response) {
                var data = response.data;
                var callbacks = [];
                return {
                    addRecord: function(record){
                        var aCopy = angular.copy(record);
                        data.push(aCopy);
                        callbacks.forEach(function(callback){
                            callback();
                        });
                    },
                    getData: function(){
                        return angular.copy(data);
                    },
                    subscribe: function(callback){
                        callbacks.push(callback);
                    }
                };
            });

        return {
            getInstance: function () {
                return thePromise;
            }
        };
    }])


    .controller('dataGridCtrl', ['$scope', 'dataStorage', 'orderByFilter', function ($scope, dataStorage, orderBy) {

        $scope.state = 'loading';
        $scope.errorMessage = null;
        var dataStorageInstance = null;

        dataStorage.getInstance()
            .then(function(instance){
                dataStorageInstance = instance;
                $scope.state = 'ready';
                updateData();
                dataStorageInstance.subscribe(onDataUpdate);
            })
            .catch(function(response){
                $scope.errorMessage = response.message;
                $scope.state = 'error';
            });

        function updateData() {
            if (!dataStorageInstance) { return; }
            $scope.data = dataStorageInstance.getData();
            orderItems();
        }

        function onDataUpdate() {
            // The data has changed.
            updateData();
        }

        function orderItems() {
            $scope.propertyName = 'firstname';
            $scope.reverse = true;
            $scope.data = orderBy($scope.data, $scope.propertyName, $scope.reverse);
        }

        $scope.sortBy = function (propertyName) {
            $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName)
                ? !$scope.reverse : false;

            $scope.propertyName = propertyName;
            $scope.data = orderBy($scope.data, $scope.propertyName, $scope.reverse);
        };

    }])

    .directive("renderDataGrid", function () {
        return {
            scope: true,
            templateUrl: 'dataGrid/Templates/grid.html'
        }
    })

    .controller('ModalCtrl', ['$scope', 'dataStorage', '$uibModal', '$log', function ($scope, dataStorage , $uibModal , $log) {
        var dataStorageInstance;

        dataStorage.getInstance()
            .then(function (instance) {
                dataStorageInstance = instance;
            })
            .catch(function (response) {
            });

        var update = function (player) {
            if (!dataStorageInstance) {
                return;
            }
            dataStorageInstance.addRecord(player);
        };

        $scope.player= {};

        $scope.sex = ['male', 'fenale'];
        $scope.tier = ['gold', 'silver','bronze'];

        $scope.open = function () {

            var modalInstance = $uibModal.open({
                templateUrl: 'dataGrid/Templates/modal.html',
                controller: 'ModalInstanceCtrl',
                resolve : {
                 sex: function () {
                    return $scope.sex;
                  },
                 tier: function () {
                    return $scope.tier;
                }
                }
            });

            modalInstance.result.then(function (response) {
                $scope.player = response;
                update($scope.player);
                $log.info('Modal dismissed at: ' + response.firstname);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };


    }])


    .controller('ModalInstanceCtrl', ['$scope', '$uibModalInstance', 'sex','tier', function ($scope, $uibModalInstance ,sex,tier) {
        $scope.player = {};
        $scope.sex=sex;
        $scope.tier=tier;

        $scope.ok = function (player) {
            $uibModalInstance.close($scope.player);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }])
;