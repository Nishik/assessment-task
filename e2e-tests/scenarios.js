'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /dataGrid when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/dataGrid");
  });


  describe('view1', function() {

    beforeEach(function() {
      browser.get('index.html#!/dataGrid');
    });


    it('should render dataGrid when user navigates to /dataGrid', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 1/);
    });

  });


  describe('view2', function() {

    beforeEach(function() {
      browser.get('index.html#!/modalPopUp');
    });


    it('should render modalPopUp when user navigates to /modalPopUp', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 2/);
    });

  });
});
